package com.gwtTest.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Element;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.user.datepicker.client.DatePicker;

/**
 * Author: siziyman
 * Date: 29.06.2017.
 */
public class BookEditor extends Composite {

    private static BookEditorUiBinder uiBinder = GWT.create(BookEditorUiBinder.class);

    @UiField (provided = true)
    LongBox idBox;
    @UiField (provided = true)
    TextBox nameBox;
    @UiField (provided = true)
    TextBox authorBox;
    @UiField (provided = true)
    DatePicker datePicker;
    @UiField (provided = true)
    TextBox timeBox;
    @UiField (provided = true)
    Button submitButton;

    public BookEditor (Book book) {
        initIdBox();
        initNameBox();
        initAuthorBox();
        initTimeBox();
        idBox.setValue(book.getId());
        nameBox.setValue(book.getName());
        authorBox.setValue(book.getAuthor());
        timeBox.setValue(DateTimeFormat.getFormat("HH:mm").format(book.getReleaseDate()));
        datePicker = new DatePicker();
        datePicker.setValue(book.getReleaseDate());
        submitButton = new Button();
        initWidget(uiBinder.createAndBindUi(this));
    }

    private void initTimeBox () {
        timeBox = new TextBox();
        timeBox.addStyleName("custom");
        Element timeElem = timeBox.getElement();
        timeElem.setPropertyBoolean("required", true);
        timeElem.setPropertyString("type", "time");
    }

    private void initAuthorBox () {
        authorBox = new TextBox();
        authorBox.addStyleName("custom");
        Element authElem = authorBox.getElement();
        authElem.setPropertyString("pattern", ".{1,}");
        authElem.setPropertyBoolean("required", true);
        authElem.setPropertyString("placeholder", "Автор");
    }

    private void initNameBox () {
        nameBox = new TextBox();
        nameBox.addStyleName("custom");
        Element nameElem = nameBox.getElement();
        nameElem.setPropertyString("pattern", ".{1,}");
        nameElem.setPropertyBoolean("required", true);
        nameElem.setPropertyString("placeholder", "Имя");
    }

    private void initIdBox () {
        idBox = new LongBox();
        idBox.addStyleName("custom");
        Element idElem = idBox.getElement();
        idElem.setPropertyString("placeholder", "ID");
        idElem.setPropertyString("type", "number");
    }

    interface BookEditorUiBinder extends UiBinder <Widget, BookEditor> {}
}
