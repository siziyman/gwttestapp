package com.gwtTest.client;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;

/**
 * Author: siziyman
 * Date: 29.06.2017.
 */

public class BookService {
    private ArrayList <Book> books;

    public BookService () {
        this.books = new ArrayList <>();
    }

    public BookService (ArrayList <Book> books) {
        this.books = books;
    }

    public ArrayList <Book> getBooks () {
        return books;
    }

    public void setBooks (ArrayList <Book> books) {
        this.books = books;
    }

    public void add (@NotNull Book book) {
        books.add(book);
    }
}