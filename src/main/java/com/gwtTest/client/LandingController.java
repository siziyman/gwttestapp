package com.gwtTest.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.event.dom.client.DoubleClickEvent;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.ui.RootLayoutPanel;

import java.util.Date;

public class LandingController implements EntryPoint {

    private static BookDataGrid bookDataGrid = new BookDataGrid();

    public void onModuleLoad () {

        bookDataGrid.getDataGrid().addDomHandler(doubleClickEvent -> {

            BookEditor bookEditor = new BookEditor(bookDataGrid.getSelected());

            bookEditor.submitButton.addClickHandler(event -> {
                Book book = bookDataGrid.getSelected();
                Book newBook = new Book(book.getId(), book.getName(), book.getAuthor(), book.getReleaseDate());
                newBook.setId(bookEditor.idBox.getValue());
                newBook.setName(bookEditor.nameBox.getValue());
                newBook.setAuthor(bookEditor.authorBox.getValue());
                Date date = DateTimeFormat.getFormat("yyyy MMM dd HH:mm")
                        .parse(DateTimeFormat.getFormat(DateTimeFormat.PredefinedFormat.DATE_MEDIUM)
                                .format(bookEditor.datePicker.getValue()) + " " + bookEditor.timeBox.getValue());
                newBook.setReleaseDate(date);
                int index = bookDataGrid.getBookService().getBooks().indexOf(book);
                bookDataGrid.getBookService().getBooks()
                        .set(index, newBook);
                RootLayoutPanel.get().clear();
                //what a crutch
                bookDataGrid.dataGrid.setRowData(bookDataGrid.getBookService().getBooks());
                RootLayoutPanel.get().add(bookDataGrid);
            });
            RootLayoutPanel.get().clear();
            RootLayoutPanel.get().add(bookEditor);
        }, DoubleClickEvent.getType());
        RootLayoutPanel.get().add(bookDataGrid);
    }
}
