package com.gwtTest.client;

import com.google.gwt.cell.client.TextCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.DataGrid;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.SingleSelectionModel;

import java.util.ArrayList;

public class BookDataGrid extends Composite {

    private static BookDataGridUiBinder uiBinder = GWT
            .create(BookDataGridUiBinder.class);

    public DataGrid <Book> getDataGrid () {
        return dataGrid;
    }

    public void setDataGrid (DataGrid <Book> dataGrid) {
        this.dataGrid = dataGrid;
    }

    @UiField (provided = true)
    DataGrid <Book> dataGrid;
    private Book selected;
    private Column <Book, String> nameColumn;
    private Column <Book, String> authorColumn;
    private Column <Book, String> IdColumn;
    private Column <Book, String> DateColumn;

    public BookService getBookService () {
        return bookService;
    }

    public void setBookService (BookService bookService) {
        this.bookService = bookService;
    }

    private BookService bookService;

    public BookDataGrid () {
        bookService = new BookService();
        initTestData();
        initDataGrid();
        initWidget(uiBinder.createAndBindUi(this));
    }

    private void initTestData () {
        ArrayList <Book> books = bookService.getBooks();
        books.add(new Book(15000L, "Преступление и наказание", "Ф. Достоевский"));
        books.add(new Book(1L, "Война и мир", "Л. Толстой"));
        books.add(new Book(2L, "Руслан и Людмила", "А. Пушкин"));
        books.add(new Book(3L, "Капитанская дочка", "А. Пушкин"));
        books.add(new Book(4L, "Евгений Онегин", "А. Пушкин"));
        books.add(new Book(5L, "Повести покойного Ивана Петровича Белкина", "А. Пушкин"));
        books.add(new Book(6L, "Дубровский", "А. Пушкин"));
        books.add(new Book(7L, "Пиковая дама", "А. Пушкин"));
        books.add(new Book(8L, "Истории Пугачёва", "А. Пушкин"));
        books.add(new Book(9L, "Сказка о царе Салтане, о сыне его славном и могучем богатыре князе Гвидоне " +
                "Салтановиче и о прекрасной царевне лебеди", "А. Пушкин"));
        books.add(new Book(10L, "Сказка о рыбаке и рыбке", "А. Пушкин"));
        books.add(new Book(11L, "Жених", "А. Пушкин"));
        books.add(new Book(12L, "Идиот", "Ф. Достоевский"));
        books.add(new Book(13L, "Игрок", "Ф. Достоевский"));
        books.add(new Book(14L, "Подросток", "Ф. Достоевский"));
        books.add(new Book(15L, "Бесы", "Ф. Достоевский"));
        books.add(new Book(16L, "Бедные люди", "Ф. Достоевский"));
        {
            String s = "TestBook";
            String a = "TestAuthor";
            for (int i = 17; i < 60; i++) {
                books.add(new Book((long) i, s + i, a + i));
            }
        }
        books.sort((o1, o2) -> {
            long res = o1.getId() - o2.getId();
            if (res < 0) {
                return -1;
            } else if (res > 0) {
                return 1;
            }
            return 0;
        });
    }

    private void initDataGrid () {
        final SingleSelectionModel <Book> selectionModel = new SingleSelectionModel <>();
        dataGrid = new DataGrid <>();
        initColumns();
        dataGrid.addColumn(IdColumn, "ID");
        dataGrid.addColumn(nameColumn, "Название");
        dataGrid.addColumn(authorColumn, "Автор");
        dataGrid.addColumn(DateColumn, "Дата добавления");
        dataGrid.setWidth("100%");
        dataGrid.setRowData(bookService.getBooks());
        selectionModel.addSelectionChangeHandler(selectionChangeEvent -> selected = selectionModel.getSelectedObject());
        dataGrid.setSelectionModel(selectionModel);

    }

    private void initColumns () {
        IdColumn = new Column <Book, String>(new TextCell()) {
            @Override public String getValue (Book book) {
                return book.getId().toString();
            }
        };
        nameColumn = new Column <Book, String>(new TextCell()) {
            @Override public String getValue (Book book) {
                return book.getName();
            }
        };
        authorColumn = new Column <Book, String>(new TextCell()) {
            @Override public String getValue (Book book) {
                return book.getAuthor();
            }
        };
        DateColumn = new Column <Book, String>(new TextCell()) {
            @Override public String getValue (Book book) {
                return DateTimeFormat.getFormat(DateTimeFormat.PredefinedFormat.DATE_MEDIUM).format(book.getReleaseDate());
            }
        };
    }

    public Book getSelected () {
        return selected;
    }

    public void setSelected (Book selected) {
        this.selected = selected;
    }


    interface BookDataGridUiBinder extends UiBinder <Widget, BookDataGrid> {
    }


}
