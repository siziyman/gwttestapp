package com.gwtTest.client;


import java.util.Date;


/**
 * Author: siziyman
 * Date: 29.06.2017.
 */

public class Book {
    private Long id;

    @Override public boolean equals (Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Book)) {
            return false;
        }

        Book book = (Book) o;

        if (getId() != null ? !getId().equals(book.getId()) : book.getId() != null) {
            return false;
        }
        if (getName() != null ? !getName().equals(book.getName()) : book.getName() != null) {
            return false;
        }
        if (getAuthor() != null ? !getAuthor().equals(book.getAuthor()) : book.getAuthor() != null) {
            return false;
        }
        return getReleaseDate() != null ? getReleaseDate().equals(book.getReleaseDate()) : book
                .getReleaseDate() == null;
    }

    @Override public int hashCode () {
        int result = getId() != null ? getId().hashCode() : 0;
        result = 31 * result + (getName() != null ? getName().hashCode() : 0);
        result = 31 * result + (getAuthor() != null ? getAuthor().hashCode() : 0);
        result = 31 * result + (getReleaseDate() != null ? getReleaseDate().hashCode() : 0);
        return result;
    }

    private String name;
    private String author;

    //sadly, Java 8 time classes aren't presented in JRE Emulation Library
    private Date releaseDate;

    public Book () {}

    public Book (Long id, String name, String author) {
        this(id, name, author, new Date());
    }

    public Book (Long id, String name, String author, Date releaseDate) {
        this.id = id;
        this.name = name;
        this.author = author;
        this.releaseDate = releaseDate;
    }

    public Long getId () {
        return id;
    }

    public void setId (Long id) {
        this.id = id;
    }

    public String getName () {
        return name;
    }

    public void setName (String name) {
        this.name = name;
    }

    public String getAuthor () {
        return author;
    }

    public void setAuthor (String author) {
        this.author = author;
    }

    public Date getReleaseDate () {
        return releaseDate;
    }

    public void setReleaseDate (Date releaseDate) {
        this.releaseDate = releaseDate;
    }
}